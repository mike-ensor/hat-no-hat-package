# Overview

This repostiory is used to deploy the Hat no Hat demonstration on a ConfigSync enabled GDC Connected (Server or Software-Only) or GKE cluster

> :warning: NOTE -- Part of the configuration is defined in the Primary Root Repository so the primary package does not need to define all varaibles

## How to use

Must apply the following code snippet. This code will create a new `RepoSync` object pointing to the Cluster Trait Repo, and create an `ExternalSecret` that allows the `RootSync` to reference a private repository.

> :note: This can be applied via ACM updates IF the Primary Root Repo is `unstructured`, otherwise a manual `kubectl apply -f <file.yaml>` is required to install this CTR.

```yaml
apiVersion: configsync.gke.io/v1beta1
kind: RootSync
metadata:
  name: hat-no-hat-pkg
  namespace: config-management-system
spec:
  sourceFormat: "unstructured"
  git:
    repo: "https://gitlab.com/mike-ensor/hat-no-hat-package.git"
    branch: "main"
    dir: "/config"
    auth: "token"
    secretRef:
      name: hat-no-hat-pkg-git-creds            # matches the ExternalSecret spec.target.name below

---

apiVersion: external-secrets.io/v1beta1
kind: ExternalSecret
metadata:
  name: hat-no-hat-pkg-git-creds-es
  namespace: config-management-system
  annotations:
    # Allow KRM in this repo to be removed if the RepoSync is removed
    configsync.gke.io/deletion-propagation-policy: Foreground
spec:
  refreshInterval: 1m
  secretStoreRef:
    kind: ClusterSecretStore
    name: gcp-secret-store
  target:                                       # K8s secret definition
    name: hat-no-hat-pkg-git-creds              ############# Matches the secretRef above
    creationPolicy: Owner
  data:
  - secretKey: username                         # K8s secret key name inside secret
    remoteRef:
      key: hat-no-hat-pkg-access-token-creds    #  GCP Secret Name
      property: username                        # field inside GCP Secret
  - secretKey: token                            # K8s secret key name inside secret
    remoteRef:
      key: hat-no-hat-pkg-access-token-creds    #  GCP Secret Name
      property: token                           # field inside GCP Secret

```

### Create GCP Secret for git-creds

Create the GCP Secret Manager secret used by `ExternalSecret` to proxy for K8s `Secret`

```
export PROJECT_ID=<your google project id>
export SCM_TOKEN_TOKEN=<your gitlab personal-access token value>
export SCM TOKEN_USER=<your gitlab personal-access token user>

gcloud secrets create hat-no-hat-pkg-access-token-creds --replication-policy="automatic" --project="${PROJECT_ID}"
echo -n "{\"token\"{{':'}} \"${SCM_TOKEN_TOKEN}\", \"username\"{{':'}} \"${SCM_TOKEN_USER}\"}" | gcloud secrets versions add hat-no-hat-pkg-access-token-creds --project="${PROJECT_ID}" --data-file=-

```

# Variations

These are common variants that would be placed in each cluster since the values vary by
each cluster. Future ConfigSync functionality will account for variants directly, until
such time, variations should be added with the `RepoSync` or `RootSync` in the
`PrimaryRootRepo`


## Local Validation

Assuming `nomos` is installed (via `gcloud components install nomos`)

```
nomos vet --no-api-server-check --path config/
```

### Docker method

Using this link to find the version of nomos-docker:  https://cloud.google.com/anthos-config-management/docs/how-to/updating-private-registry#expandable-1

```
docker pull gcr.io/config-management-release/nomos:stable
docker run -it -v $(pwd):/code/ gcr.io/config-management-release/nomos:stable nomos vet --no-api-server-check --path /code/config/
```

### ConfigSync Overview

See [our documentation](https://cloud.google.com/anthos-config-management/docs/repo) for how to use each subdirectory.

## Series Links

This project is one component in a multi-component solution. The composed parts make up the game "Price a Tray".

https://gitlab.com/mike-ensor/hat-no-hat-frontend
: This is the User Experience and User Interface for the game

https://gitlab.com/mike-ensor/price-a-tray-inference-server
: This is the inference server configured to use a hat-no-hat verioned .tflite model

https://gitlab.com/mike-ensor/rtsp-to-mjpeg.git
: This project turns a RTSP stream into a MJPEG stream for consumption by the UI

https://gitlab.com/mike-ensor/light-tower-interface.git
: This project is an interface for a light tower based on X410 Web Controller Relay

https://gitlab.com/mike-ensor/hat-no-hat-package
: This is the project used to deploy to a Kubernetes project with ConfigSync installed
